module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  onboardingConfig: {
    "extends": [
      "github>platform-engineering-org/.github"
    ]
  },
  hostRules: [
    {
      matchHost: 'github.com',
      token: process.env.GITHUB_COM_TOKEN,
    }
  ],
  autodiscover: true,
  autodiscoverNamespaces: ["centos/automotive/container-images", "bootc-org", "platform-engineering-org", "centos/automotive/platform-engineering"]
};
